<?php
	class CloudeNine {
		
		var $url;
		var $site;
		var $html;
		var $template;
		var $mysql;
		var $usercontrol;
		var $FE;
		var $BE;
		
		var $PageConfig = array();
		var $PageObj = array();
		
		
		/**
		* Starts the Application
		*
		* @return	none
		* @since	Version 0.2
		**/
		function run() {
		
			// Checking PHP version
			if (version_compare(phpversion(), '5.3', '<'))	die ('CloudNine requires PHP 5.2.0 or higher.');

			include("classes/c9.define.class.php");
			SystemInfo("---------Starting---------", "c9 (run)");
			SystemInfo("_private_dir: "._private_dir,"define");
			SystemInfo("_include_dir: "._include_dir,"define");
			SystemInfo("_template_dir: "._template_dir,"define");
			
			
			
			// Define Variables and Render URL
			$this->RequireFile(_include_dir."/c9.url.class.php", 1);$this->url = new url;
			//If no Errors where found include the "page.php" or any other
			if($this->url->URL[2][0]){
				$this->PageObj['url'] = $this->url->URL[2];
				unset($this->PageObj['url'][0]);
				$this->RequireFile(constant($this->url->URL[2][0]), 1);
				$page = new $this->url->URL[2][0];
				
				if(!method_exists($this->url->URL[2][0],'GetConfig'))
					SystemWarning("GetConfig in ".$this->url->URL[2][0]." not found.", "c9 (run)");
				
				elseif($page->GetConfig()){ 
					define("_pagesDir", _page_dir."/".$this->url->URL[2][0]);
					
					// configure and load desired Modules
					if($page->PageConfig['Template']){
						$this->RequireFile(_include_dir."/c9.template.class.php", 1);
						$this->PageObj['template'] = $this->template = new template;
						$this->template->Config['Language'] = $page->PageConfig['PageLanguage'];
					}
					if($page->PageConfig['HtmlClass']){
						$this->RequireFile(_include_dir."/c9.html.class.php", 1);
						$this->RequireFile(_include_dir."/c9.lang.class.php", 1); language::init();
						$this->PageObj['site'] = $this->site = new site; 
						$this->PageObj['html'] = $this->html = new html;
					}
					if($page->PageConfig['MySQL']){
						$this->RequireFile(_include_dir."/c9.mysql.class.php", 1);
						$this->PageObj['mysql'] = $this->mysql = new mysql;
						$this->PageObj['mysqlcon'] = $this->mysql->mysqlStart();
					}
					if($page->PageConfig['uc']){
						if(!$page->PageConfig['MySQL'])
							$this->RequireFile(_include_dir."/c9.mysql.class.php", 1);
						$this->RequireFile(_include_dir."/c9.usercontrol.class.php", 1);
						$this->PageObj['uc'] = $this->usercontrol = new usercontrol;
						$this->PageObj['ucUser'] = $this->usercontrol->ucstart();
					}
					if($page->PageConfig['cms']){
						if(!$page->PageConfig['MySQL'] && !$page->PageConfig['uc'])
							$this->RequireFile(_include_dir."/c9.mysql.class.php", 1);
						if(!$page->PageConfig['HtmlClass']){
							$this->RequireFile(_include_dir."/c9.html.class.php", 1);
							$this->RequireFile(_include_dir."/c9.lang.class.php", 1); language::init();
						}
						$this->RequireFile(_include_dir."/c9.cms.class.php", 1);
						$this->PageObj['FE'] = $this->FE = new frontend($this->url->URL[2][0]);
						$this->PageObj['BE'] = $this->BE = new backend($this->url->URL[2][0]);
					}
				}
				
				
				// Funktionsaufruf f�r Clientpage
				if(!method_exists($this->url->URL[2][0],'GO'))
					SystemWarning("GO in ".$this->url->URL[2][0]." not found.", "c9 (run)");
				
				else $page->GO(&$this->PageObj);
				
				if($page->PageConfig['Template']) $this->template->RenderPage($page);
				
			}else SystemError("An Error is orrured becaused of ".$this->url->URL[2][0], "c9 (run)");
			
			
			SystemInfo("This page was created in ".(microtime(true) - $_SERVER['REQUEST_TIME'])." seconds", "c9 (run)",1); 
			SystemInfo("----------Ending----------\n\n", "c9 (run)");
			
		
		}	
		
		
		/**
		* Requires any File
		*
		* @param	string
		* @return	none
		* @since	Version 0.2
		**/		
		static function RequireFile($file, $systemfile = FALSE){
			
			if(is_readable(_private_dir.$file)){
				if(require(_private_dir.$file)){
					SystemInfo("RequireFile "._private_dir.$file." [OK]", "c9 (RequireFile)");
					return true;
				}else{
					SystemError("RequireFile "._private_dir.$file." [faild to require]", "c9 (RequireFile)");
				}
			}else{
				if($systemfile)
					SystemError("RequireFile "._private_dir.$file." [systemfile not found]", "c9 (RequireFile)");
				else
					SystemWarning("RequireFile "._private_dir.$file." [file not found]", "c9 (RequireFile)");
				
			}
		}	
				
	}
	

	
	function SystemErrorHandler($code, $msg, $file, $line, $context){
		switch($code){
			case 2:
			case 4:
			case 32:
			case 124:		 
				SystemWarning("\"".$msg."\" in ".$file." on line ".$line);
				break;	
			case 8:	
			case 1024:
			case 2047:
			case 2048:		
				SystemInfo("\"".$msg."\" in ".$file." on line ".$line);
				break;
			case 1:
			case 16:
			case 64:
			case 256:	
				SystemError("\"".$msg."\" in ".$file." on line ".$line);
				break;
			
		}
		
		
	}
	function SystemError($error, $func = FALSE, $logging = FALSE){
		while(strlen($func)<25){
			$func .= " ";
		}
		if(_logging == 0 || _logging == 1 || _logging == 2 || _logging == 3 || $logging){
			$handler = fOpen('log.txt' , "a+");
	 		fWrite($handler , "\n[EE][".$func."][".date("d.m.Y  H:i:s",time())."]\t".$error);
	 		fClose($handler);
		}
		ShowErrorPage("500");
		exit;	
	}
	
	
	function SystemWarning($warning, $func = FALSE, $logging = FALSE){
		while(strlen($func)<25){
			$func .= " ";
		}
		if(_logging == 1 || _logging == 2  || $logging){
			$handler = fOpen('log.txt' , "a+");
	 		fWrite($handler , "\n[WW][".$func."][".date("d.m.Y  H:i:s",time())."]\t".$warning);
	 		fClose($handler);
		}
	}
	
	function SystemInfo($information, $func = FALSE, $logging = FALSE){
		while(strlen($func)<25){
			$func .= " ";
		}
		if(_logging == 2 || ($logging && _logging == 3)){
			$handler = fOpen('log.txt' , "a+");
	 		fWrite($handler , "\n[II][".$func."][".date("d.m.Y  H:i:s",time())."]\t".$information);
	 		fClose($handler);
		}
	}
	
	function ShowErrorPage($error){
	
		// Setup
		$email = 'example@example.com';  //Change to your e-mail address

		// Get Variables
		$requested_url = $_SERVER['REQUEST_URI'];
		$server_name = $_SERVER['SERVER_NAME'];
		$subject2 = "IP ONLY";

		switch ($error) {
			# Error 400 - Bad Request
			case 400:
			$errordesc = '<h1>Bad Request</h1>
			  <h2>Error Type: 400</h2>
			  <p>The URL that you requested does not exist on this server. You might want to re-check the spelling and the path.</p>';
			break;

			# Error 401 - Authorization Required
			case 401:
			$errordesc = '<h1>Authorization Required</h1>
			  <h2>Error Type: 401</h2>
			  <p>The URL that you requested requires pre-authorization to access.</p>';
			break;

			# Error 403 - Access Forbidden
			case 403:
			$errordesc = '<h1>Access Forbidden</h1>
			  <h2>Error Type: 403</h2>
			  <p>Access to the URL that you requested is forbidden.</p>';
			break;

			# Error 404 - Page Not Found
			case 404:
			$errordesc = '<h1>File Not Found</h1>
			  <h2>Error Type: 404</h2>
			  <p>Ooops! The page you are looking for cannot be found. This may be because:</p>
			  <ul>
				<li>the path to the page was entered wrong;</li>
				<li>the page no longer exists; or</li>
				<li>there has been an error on the Web site.</li>
			  </ul>';
			break;

			# Error 500 - Server Configuration Error
			case 500:
			$errordesc = '<h1>Server Configuration Error</h1>
			  <h2>Error Type: 500</h2>
			  <p>The URL that you requested resulted in a server configuration error. It is possible that the condition causing the problem will be gone by the time you finish reading this.</p>';
			
			break;

			# Unknown error
			default:
			$errordesc = '<h2>Unknown Error</h2>
			  <p>The URL that you requested resulted in an unknown error. It is possible that the condition causing the problem will be gone by the time you finish reading this. </p>';

			}

			// Display selected error message
			echo($errordesc);			
			exit;
	}
?>