<?php
	date_default_timezone_set('Europe/Paris');

	define("_private_dir", str_replace("\\", "/" ,dirname(dirname(__FILE__)))."/");
	define("_logging", "2");
	
	//Language
	define("_defLang", "de");
	
	define("_Language", _defLang );
	
	
	// Dirs
	define("_include_dir", "classes");
	define("_template_dir", "templates");
	define("_page_dir", "module");

	//DB
	define("_db_host", "localhost");
	define("_db_user", "c9");
	define("_db_pass", "start");
	define("_db_database", "c9");
	define("_db_duration", false);
	
	//Comment
	define("_QuelltextCommentHeader", 
"<!--
			~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				Martin Eigenmann kaph.ch
			~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	This website is powered by CloudNine - Let's create pages on the easy way!
	
-->");

	define("_DocType", "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
	
	//pages
	define("default", "page");
	define("page", _page_dir."/page.php");
	//define("files", "/page/files.php");
	//define("test", "/page/test.php");
	
	
	

	
?>
