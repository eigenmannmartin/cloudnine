<?php
	class site {
		
		var $elements = array();
		var $site = array();
		var $line = 0;
			


		function site(){
			SystemInfo("class site starting", "site (site)");
		}
		
		
		/**
		* Add Text to $elements
		*
		* @param	string
		* @return	none
		* @since	Version 0.1
		**/

		function AddText($text) {
			$this->elements[$this->line] = $text;
			$this->line++;
		}
		


		/**
		* Erases whole $elements
		*
		* @param	none
		* @return	none
		* @since	Version 0.1
		**/
		function EraseElements() {
			for($i = 0; $i < $this->line; $i++) {
				$this->elements[$i] = NULL;
			}			
			$this->line = 0;
		}
		


		/**
		* Genetates site Array
		*
		* @param	array
		* @return	true
		* @since	Version 0.1
		**/
		function GenerateSite($content = NULL) {
		
			if($content === NULL){
				$content = $this->elements;
			}

			$counter = 0;
			foreach($content as &$value) {
				if(is_array($value)){
					foreach($value as &$val){
						$this->site[$counter] = $val;
						$counter++;
					}
				} else {
					$this->site[$counter] = $value;
					$counter++;
				}
			}
			return true;
		}
		


		/**
		* Returns the site Array
		*
		* @param	none
		* @return	none
		* @since	Version 0.1
		**/
		function SiteToString($content = "") {
			foreach($this->site as &$value)
				$content.= $value."\n";
			unset($this->site);
			return $content;
		}
		
		
		
		
		function SetHeaders($file, $forceDownload = 1){

		    //First, see if the file exists
		    if (!is_file($file)) return FALSE;
		
		    //Gather relevent info about file
		    $len = filesize($file);
		    $filename = basename($file);
		    $file_extension = strtolower(substr(strrchr($filename,"."),1));
		
		    //This will set the Content-Type to the appropriate setting for the file
		    switch( $file_extension ) {
		      case "pdf": $ctype="application/pdf"; break;
		      case "exe": $ctype="application/octet-stream"; break;
		      case "zip": $ctype="application/zip"; break;
		      case "doc": $ctype="application/msword"; break;
		      case "xls": $ctype="application/vnd.ms-excel"; break;
		      case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
		      case "gif": $ctype="image/gif"; break;
		      case "png": $ctype="image/png"; break;
		      case "jpeg":
		      case "jpg": $ctype="image/jpg"; break;
		      case "mp3": $ctype="audio/mpeg"; break;
		      case "wav": $ctype="audio/x-wav"; break;
		      case "mpeg":
		      case "mpg":
		      case "mpe": $ctype="video/mpeg"; break;
		      case "mov": $ctype="video/quicktime"; break;
		      case "avi": $ctype="video/x-msvideo"; break;
		      case "css": $ctype="text/css"; break;
		      case "txt": $ctype="text/txt"; break;
		
		      //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
		      case "php":
		      case "htm":
		      case "html": $ctype = FALSE; break;
		
		      default: $ctype="application/force-download";
		    }
		
		    //Begin writing headers
		    header("Pragma: public");
		    header("Expires: 0");
		    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		    header("Cache-Control: public");
		    header("Content-Description: File Transfer");
		   
		    //Use the switch-generated Content-Type
		    header("Content-Type: $ctype");
		
		    //Force the download
		    if($forceDownload){
		   		header("Content-Disposition: attachment; filename=".$filename.";" );
		    	header("Content-Transfer-Encoding: binary");
		    }
		    header("Content-Length: ".$len);

		}
		
		
	}
	
	
	

	class html EXTENDS site{
		var $div;

		function html(){
			SystemInfo("class html starting", "html (html)");
			
		}

		/**
		* Generates HTML Tags
		*
		* @param	string		div,body,head,html
		* @param	string/array
		* @param	string		<tag class="">
		* @param	string		<tag id="">
		* @return	array
		* @since	Version 0.1
		**/
		function CreateElement($element,$content,$class = "",$id = "",$method = "",$action = "",$name = "",$legend = "") {
			
			switch($element) {
				case 'div':
					include(_private_dir._template_dir."/elements/div.tpl");
					return $div;
				case 'form':
					include(_private_dir._template_dir."/elements/form.tpl");
					return $form;
				case 'table':
					include(_private_dir._template_dir."/elements/table.tpl");
					return $table;
				case 'body':
					include(_private_dir._template_dir."/elements/body.tpl");
					return $body;
				case 'head':
					include(_private_dir._template_dir."/elements/head.tpl");
					return $head;
				case 'html':
					include(_private_dir._template_dir."/elements/html.tpl");
					return $html;
				case 'fieldset':
					include(_private_dir._template_dir."/elements/fieldset.tpl");
					return $fieldset;
				case 'ul':
					include(_private_dir._template_dir."/elements/ul.tpl");
					return $ul;
				case 'ol':
					include(_private_dir._template_dir."/elements/ol.tpl");
					return $ol;
				default:
					return FALSE;
				
			}
		}
		


		/**
		* Generates HTML Tags
		*
		* @param	string		h1,h2,h3,h4,h5,h6,p,title
		* @param	string
		* @param	string		<tag class="">
		* @param	string		<tag id="">
		* @param	string		css file
		* @return	array
		* @since	Version 0.1
		**/
		function CreateText($element,$text,$class = "",$id = "",$datacss="",$type = "",$name = "",$value = "", $httpequiv =""){
			switch($element){
				case 'h1':
					include(_private_dir._template_dir."/text/h1.tpl");
					return $h1;
				case 'h2':
					include(_private_dir._template_dir."/text/h2.tpl");
					return $h2;
				case 'h3':
					include(_private_dir._template_dir."/text/h3.tpl");
					return $h3;
				case 'h4':
					include(_private_dir._template_dir."/text/h4.tpl");
					return $h4;
				case 'h5':
					include(_private_dir._template_dir."/text/h5.tpl");
					return $h5;
				case 'h6':
					include(_private_dir._template_dir."/text/h6.tpl");
					return $h6;
				case 'p':
					include(_private_dir._template_dir."/text/p.tpl");
					return $p;
				case 'title':
					include(_private_dir._template_dir."/text/title.tpl");
					return $title;
				case 'css':
					include(_private_dir._template_dir."/text/css.tpl");
					return $css;
				case 'input':
					include(_private_dir._template_dir."/text/input.tpl");
					return $input;
				case 'li':
					include(_private_dir._template_dir."/text/li.tpl");
					return $li;
				case 'meta':
					include(_private_dir._template_dir."/text/meta.tpl");
					return $meta;
				case 'link':
					include(_private_dir._template_dir."/text/link.tpl");
					return $link;
				case 'text':
					return $text;
				default:
					return FALSE;
			}
		}
		
		
		
		
		
		
		function CreateTemplate($template, $data = NULL, $lang = _defLang){
			
			switch($template){
				case 'login': return $this->CreateLogin($data,$lang);
				case 'logout': return $this->CreateLogout($data,$lang);
				default: return FALSE;
			}
			
		}
		
		private function CreateLogin($data,$lang){
			
			if(!array_key_exists('method', $data)) $data['method'] = "";
			if(!array_key_exists('action', $data)) $data['action'] = "";
			if(!array_key_exists('template', $data)) $data['template'] = '1';
			if(!array_key_exists('legend', $data)) $data['legend'] = "";
			
			if($data['template'] == '1')
				$this->GenerateSite(
					$this->CreateElement('form',
						$this->CreateElement('table', 
							array(
								array(language::getWords('username',$lang),$this->CreateText('input',FALSE,FALSE,FALSE,FALSE,'text','username')),
								array(language::getWords('password',$lang),$this->CreateText('input',FALSE,FALSE,FALSE,FALSE,'password','userpw')),
								array($this->CreateText('input',FALSE,FALSE,FALSE,FALSE,'submit',FALSE,language::getWords('send',$lang)))
							)
						)
					,FALSE,FALSE,$data['method'],$data['action'])
				);
			
			if($data['template'] == '2')
				$this->GenerateSite(
					$this->CreateElement('fieldset',
						$this->CreateElement('form',
							$this->CreateElement('table', 
								array(
									array(language::getWords('username',$lang),$this->CreateText('input',FALSE,FALSE,FALSE,FALSE,'text','username')),
									array(language::getWords('password',$lang),$this->CreateText('input',FALSE,FALSE,FALSE,FALSE,'password','userpw')),
									array($this->CreateText('input',FALSE,FALSE,FALSE,FALSE,'submit',FALSE,language::getWords('send',$lang)))
								)
							)
						,FALSE,FALSE,$data['method'],$data['action'])
					,FALSE,FALSE,FALSE,FALSE,FALSE,'login')
				);
				
				
			return $this->SiteToString();
		}
		
			private function CreateLogout($data,$lang){
			
			if(!array_key_exists('template', $data)) $data['template'] = '1';
			if(!array_key_exists('legend', $data)) $data['legend'] = "";
			
			if($data['template'] == '1')
				$this->GenerateSite(
					$this->CreateElement('form',
						array(NULL,$this->CreateText('input',FALSE,FALSE,FALSE,FALSE,'submit',FALSE,language::getWords('logout',$lang)))
					,FALSE,FALSE,FALSE,$data['action'])
				);
			
			if($data['template'] == '2')
				$this->GenerateSite(
					$this->CreateElement('fieldset',
						$this->CreateElement('form',
							array(NULL,$this->CreateText('input',FALSE,FALSE,FALSE,FALSE,'submit',FALSE,language::getWords('logout',$lang)))
						,FALSE,FALSE,FALSE,$data['action'])
					,FALSE,FALSE,FALSE,FALSE,FALSE,'logout')
				);
				
			return $this->SiteToString();
		}

	
	}
	



	/**
	* indent HTML Tags
	*
	* @param	array		used for output
	* @param	array/string	input Array
	* @return	none
	* @since	Version 0.1
	**/
	function SimplifyArray(&$content,$array,$ebene,&$counter) {
		if(is_array($array)){
			foreach ($array as $value) {
				if(is_array($value)) {
					SimplifyArray($content,$value,$ebene+1, $counter);
				} else {
					$content[$counter] = "";
					for($i = 0; $i < $ebene; $i++) $content[$counter] .= "\t";
					$content[$counter] .= $value;
					$counter++;
				}
			}
		} else {
			$content[$counter] = $array;
			$counter++;
		}		
		
	}	
	
?>
