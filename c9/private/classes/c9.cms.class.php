<?php 

/*
/////** for class frontend
--
-- Datenbank: `c9`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `page_content`
--

CREATE TABLE IF NOT EXISTS `page_content` (
  `ELEMENT_ID` int(16) NOT NULL AUTO_INCREMENT,
  `ELEMENT_TYPE` char(12) NOT NULL,
  `PAGE_ID` int(8) NOT NULL,
  `CHILD_ELEMENT` int(16) NOT NULL,
  `BELOW_ELEMENT` int(16) NOT NULL,
  `ELEMENT_CONTENT` mediumtext NOT NULL,
  PRIMARY KEY (`ELEMENT_ID`),
  UNIQUE KEY `ELEMENT_ID` (`ELEMENT_ID`),
  KEY `PAGE_ID` (`PAGE_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

*/



	class frontend
	{	
		/** Variabeln f�r dieses Objekt */
		var $PageContent = array();
		var $PluginName = "";

		var $PAGE_DB = array();
		
		
		
		/**
		 * erstellt das mysql Objekt f�r die FE-Klasse
		 **/
		function frontend($PluginName){
			SystemInfo("class frontend starting", "fe (frontend)");	
			$this->mysql = new mysql;
			$this->mysql->mysqlStart();
			$this->site = new site; 
			$this->html = new html;
			$this->PluginName = $PluginName;
		}
		
		
		
		
		/**
		 * Sucht die URL und gibt die Seiten ID zur�ck
		 *
		 * @param	array	URL-Array aus URL-Objekt
		 **/		
		function GetPageID(&$URLArray){
			foreach($URLArray as &$URL){
				$this->URL .= $URL;
			}
			
			$result = $this->mysql->Selectquery(array('select'=>'PAGE_ID,PAGE_FIRSTELEMENT,PAGE_TITLE','from'=>'page_list','where'=>'PAGE_URL=\''.$this->URL.'\'','group'=>'','limit'=>'','order'=>''));
			if($result){
				$this->PAGE_DB = mysql_fetch_assoc($result);
			}else{
				return FALSE;
			}
			
			if(mysql_fetch_assoc($result)){
				return FALSE;
			}elseif(isset($this->PAGE_DB['PAGE_ID'])){
				return $this->PAGE_DB['PAGE_ID'];
			}else{
				return FALSE;
			}
			
		}
		
		
		function SetPageID($ID = NULL){
		
			if($ID === NULL){
				$ID = 1;
			}
		
			$result = $this->mysql->Selectquery(array('select'=>'PAGE_ID,PAGE_TITLE','from'=>'page_list','where'=>'PAGE_ID=\''.$ID.'\'','group'=>'','limit'=>'','order'=>''));
			$this->PAGE_DB = mysql_fetch_assoc($result);
			
			
			if(isset($this->PAGE_DB['PAGE_ID'])){
				return TRUE;
			} else {
				return FALSE;
			}
		}
		
		
		
		/**
		 * gibt den gesamten Seiten-Inhalt der gew�hlten Tabelle zur�ck
		 *
		 * @param	int		PAGE_ID Field in Table
		 * @param	char	TABLENAME where to search
		 **/
		function GetPageContent($CONTENT_ID = NULL, $TableName = NULL){
			if($TableName == NULL){
				$TableName_CONTENT = strtoupper($this->PluginName)."_CONTENT";
				$TableName_CONTENT_ASSOC = strtoupper($this->PluginName)."_CONTENT_ASSOC";
			}else{
				$TableName_CONTENT = $TableName."_CONTENT";
				$TableName_CONTENT_ASSOC = $TableName."_CONTENT_ASSOC";
			}
				
				
			if($CONTENT_ID == NULL){
				if(isset($this->PAGE_DB['PAGE_ID'])){
					$result = $this->mysql->Selectquery(array('select'=>'CONTENT_ID, PAGE_FIRSTELEMENT, ASSOC_NAME','from'=>$TableName_CONTENT_ASSOC ,'where'=>'PAGE_ID=\''.$this->PAGE_DB['PAGE_ID'].'\'','group'=>'','limit'=>'','order'=>''));
					while($PageContentAssocTMP = mysql_fetch_assoc($result)){
						$this->PageContentAssoc[] = $PageContentAssocTMP;
					}
					
					foreach($this->PageContentAssoc as &$ContentID){
						$result = $this->mysql->Selectquery(array('select'=>'ELEMENT_ID,ELEMENT_TYPE,CHILD_ELEMENT,BELOW_ELEMENT,ELEMENT_CONTENT','from'=>$TableName_CONTENT,'where'=>'CONTENT_ID=\''.$ContentID['CONTENT_ID'].'\'','group'=>'','limit'=>'','order'=>'ELEMENT_ID'));
						while($PageContentTMP = mysql_fetch_assoc($result)){
							$this->PageContent[$ContentID['ASSOC_NAME']][] = $PageContentTMP;
							$this->PAGE_DB['FirstElement'][$ContentID['ASSOC_NAME']] = $ContentID['PAGE_FIRSTELEMENT'];
						}
					}
				
				}else{
					return FALSE;
				}
			}
			
			
			
			if($this->PageContent !== FALSE)
				return TRUE;
			else
				return FALSE;
		}
		
		
		
		
		
		function GeneratePageContent(){
			foreach($this->PageContent as $Key => &$PageContent){
				$this->site->elements = $this->GeneratePageContent_Helper($this->PAGE_DB['FirstElement'][$Key], $PageContent);
				$this->site->GenerateSite($this->site->elements);
				$Contents[$Key] = $this->site->SiteToString();
				$this->site->EraseElements();
			}
			return $Contents;
		}
		
		
		
		
		
		
		function GeneratePageContent_Helper($Child_Element, &$PageContent){
			$end = TRUE;
			
			$site = new site; 
			$html = new html;
			
			while($end){
				foreach($PageContent as &$Element){
					if($Element['ELEMENT_ID'] == $Child_Element){
					
						$Child_Element = $Element['BELOW_ELEMENT'];
						if($Element['CHILD_ELEMENT'] !== 0){}
						$Element['ELEMENT_TYPE_EXPLODED'] = explode("-", $Element['ELEMENT_TYPE']);
						
						
						switch($Element['ELEMENT_TYPE_EXPLODED'][0]){
							case 1:
								$site->AddText($html->CreateElement($Element['ELEMENT_TYPE_EXPLODED'][1], $this->GeneratePageContent_Helper($Element['CHILD_ELEMENT'], &$PageContent)));
								break;
							case 2:
								$site->AddText($html->CreateText($Element['ELEMENT_TYPE_EXPLODED'][1], $Element['ELEMENT_CONTENT']));
								break;
							case 3:
								break;
						}
					}	
				}
				if($Child_Element == 0){
					$end = FALSE;
				}	
			}
			return $site->elements;
			
		}

	}
	
	
	
	
	
	
	
	
	
	class backend
	{	
		/** Variabeln f�r dieses Objekt */
		var $PluginName = "";
		var $LastEntryID = 0;
		var $FirstEntryID = FALSE;
		
		var $btag = '';
		
		
		
		
		
		
		/**

		**/
		function backend($PluginName){
			SystemInfo("class backend starting", "be (backend)");		
			$this->mysql = new mysql;
			$this->mysql->mysqlStart();	
			$this->site = new site; 
			$this->html = new html;
			$this->PluginName = $PluginName;			
		}
		
		
		
		function AddContent($Content, $TableName = NULL){
		
			if($TableName == NULL){
				$this->Table['CONTENT'] = strtoupper($this->PluginName)."_CONTENT";
				$this->Table['CONTENT_ASSOC'] = strtoupper($this->PluginName)."_CONTENT_ASSOC";
			}else{
				$this->Table['CONTENT'] = $TableName."_CONTENT";
				$this->Table['CONTENT_ASSOC'] = $TableName."_CONTENT_ASSOC";
			}
			
			$result = $this->mysql->Selectquery(array('select'=>'CONTENT_ID','from'=>$this->Table['CONTENT'],'group'=>'','where'=>'','limit'=>'1','order'=>'CONTENT_ID DESC'));
			$result = mysql_fetch_assoc($result);

			$this->LastEntryID = 0;
			$this->FirstEntryID = FALSE;

			foreach($Content as $val){
				$this->AddContent_Helper($val, ($result['CONTENT_ID']+1));
			}
		}
		
		
		
		function AddContent_Helper(&$Content, $ContentID){
			$CildElement = FALSE;
		
			if(is_array($Content->content)){
				foreach($Content->content as &$val){
					$this->AddContent_Helper($val, $ContentID);
					if(!$CildElement){
						$CildElement = $this->LastEntryID;
					}
				}
				$this->mysql->Insertquery($this->Table['CONTENT'], array('ELEMENT_TYPE' => '1-'.$Content->type, 'CONTENT_ID' => $ContentID, 'CHILD_ELEMENT' => $CildElement, 'BELOW_ELEMENT' => '0', 'ELEMENT_CONTENT' => ''));
				
				if($this->LastEntryID !== 0){
					$this->mysql->Updatequery(array('update' => $this->Table['CONTENT'], 'set'=>'BELOW_ELEMENT=\''.mysql_insert_id($this->mysql->serverid).'\'','where'=>'ELEMENT_ID=\''.$this->LastEntryID.'\'','limit'=>'','order'=>''));
				}
				$this->LastEntryID = mysql_insert_id($this->mysql->serverid);
			
			}else{
			
				$this->mysql->Insertquery($this->Table['CONTENT'], array('ELEMENT_TYPE' => '2-'.$Content->type, 'CONTENT_ID' => $ContentID, 'CHILD_ELEMENT' => '0', 'BELOW_ELEMENT' => '0', 'ELEMENT_CONTENT' => $Content->content));
				$LastID[1] = mysql_insert_id($this->mysql->serverid);
				
				if($this->LastEntryID !== 0){
					$this->mysql->Updatequery(array('update' => $this->Table['CONTENT'], 'set'=>'BELOW_ELEMENT=\''.mysql_insert_id($this->mysql->serverid).'\'','where'=>'ELEMENT_ID=\''.$this->LastEntryID.'\'','limit'=>'','order'=>''));
				}
				
				$this->LastEntryID = mysql_insert_id($this->mysql->serverid);
				
				if(!$this->FirstEntryID){
					$this->FirstEntryID = $this->LastEntryID;
				}
			}
			
		}
		
		
		
		
		function GenerateContent($text){
		
			$this->btag = array('pre', 'h[1-6]', 'p');
			$btags = join('|', $this->btag);
			
			$this->atag = array('<', '>', '=', '<>');
			$atags =  join('|', $this->atag);
			
			$text = str_replace("\r\n", "\n", $text);
			$text = preg_replace("/\n{3,}/", "\n\n", $text);
			$text = preg_replace("/\n *\n/", "\n\n", $text);
			$text = preg_replace('/"$/', "\" ", $text);
		
			$text = explode("\n\n", $text);

			
			foreach($text as &$paragraph){
				if (preg_match("/($btags)($atags)?\. (.*)/", $paragraph, $tag)){
					$Content[] = new Contentdatastructure($tag[1], $this->FormHTMLText($tag[3]));
				}else{
					$Content[] = new Contentdatastructure("p", $this->FormHTMLText($paragraph));
				}
			}
			
			return $Content;	
		}
		
		
		
		
		function FormHTMLText($text){
			$this->ctag = array('#', '##', '###', '####', '\*', '\*\*', '\*\*\*', '\*\*\*\*');
			$ctags = join('|', $this->ctag);
			
			$text = explode("\n", $text);
			
			
			foreach($text as &$line){
				$line = $this->FormatHTMLText_Helper($line);
				
				if(preg_match("/^($ctags)(?!$ctags) (.*)$/", $line, $match)){
					var_dump($match);
				}
			}
		}
		
		
		
		function FormatHTMLText_Helper($line){
			$spantag = array('\*\*','\*','\?\?','-','__','_','%','\+','~','\^');
			$spantanames = array('*'  => 'strong', '**' => 'b', '??' => 'cite', '_'  => 'em', '__' => 'i', '-'  => 'del', '%'  => 'span', '+'  => 'ins', '~'  => 'sub', '^'  => 'sup',);
			$pnct = ".,\"'?!;:";
			 
			foreach($spantag as $s){
			
				$line = preg_replace_callback("/
					(?:^|(?<=[\s>$pnct])|([{[]))
					($s)(?!$s)
					(?::(\S+))?
					([^\s$s]+|\S[^$s\n]*[^\s$s\n])
					([$pnct]*)
					$s
					(?:$|([\]}])|(?=[[:punct:]]{1,2}|\s))
					/x",
					create_function(
						'$matches',
						'$spantanames = array(\'*\'  => \'strong\', \'**\' => \'b\', \'??\' => \'cite\', \'_\'  => \'em\', \'__\' => \'i\', \'-\'  => \'del\', \'%\'  => \'span\', \'+\'  => \'ins\', \'~\'  => \'sub\', \'^\'  => \'sup\',);
						return "<".$spantanames[$matches[2]].">".$matches[4]."</".$spantanames[$matches[2]].">";'
					),
					
					$line);
				
				
			}
			return $line;
			// "/^(.*)($s)(?!$s)(.*)($s)(.*)$/", 
			
		}

		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	class Contentdatastructure{
		var $content	= FALSE;
		var $type		= FALSE;
		
		function Contentdatastructure($type = FALSE, $content = FALSE){
			$this->content	= $content;
			$this->type		= $type;
		}
	}

?>