<?php
	class template{
		
		//var $TemplateFile = "page/template1.html";
		var $TemplateFile;
		var $TemplateContent;
		
		
		
		/**
		* Render the Page
		*
		* @param	none
		* @return	none
		* @since	Version 0.2
		**/
		function RenderPage(&$page){
			
			$this->TemplateFile = _private_dir."/"._page_dir."/".$page->PageConfig['Template'];
			SystemInfo("using TemplateFile: ".$this->TemplateFile, "template (RenderPage)");
			if(!file_exists($this->TemplateFile)){
				SystemWarning("TemplateFile not exists: ".$this->TemplateFile, "template (RenderPage)");
				return False;
			}
			
			if($this->ReadTemplate())
				if($page->Replaces)
					foreach($page->Replaces as $title => $value)
						$this->ReplaceTemplate($title, $value);
				else
					SystemWarning("Invalid replaces", "template (RenderPage)");
			else return FALSE;
			
			$this->AddComment();
			$this->AddDocType();
			
			echo $this->TemplateContent;
			return TRUE;
		}
		
		
		function AddComment(){
			if(defined('_QuelltextCommentHeader')){
				$this->TemplateContent = str_replace("<head>", "<head>\n"._QuelltextCommentHeader, $this->TemplateContent);
			}
		}
		
		
		function AddDocType(){
		
			$DocXmlns = "xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$this->Config['Language']."\" lang=\"".$this->Config['Language']."\"";
		
			if(defined('_DocType')){
				$this->TemplateContent = str_replace("<html>", _DocType."\n<html>", $this->TemplateContent);
			}
			if(isset($this->Config['Language'])){
				$this->TemplateContent = str_replace("<html>", "<html ".$DocXmlns." >", $this->TemplateContent);
			}
		}
		
		
		
		
		/**
		* Read the TemplateFile in $TemplateContent
		*
		* @param	none
		* @return	none
		* @since	Version 0.2
		**/
		function ReadTemplate() {
			$file = @fopen($this->TemplateFile, "r");
			if($file){
				while(!feof($file)) {
					$temp = fgets($file, 4096);
					$this->TemplateContent .= $temp;
				}
				SystemInfo("using TemplateFile: ".$this->TemplateFile, "template (ReadTemplate)");
				return TRUE;
			}else{
				SystemWarning("could not read TemplateFile: ".$this->TemplateFile, "template (ReadTemplate)");
				return FALSE;
			}
		}
		
		
		
		/**
		* Replaces the Placeholder
		*
		* @param	string	title to replace
		* @param	string
		* @return	none
		* @since	Version 0.2
		**/		
		function ReplaceTemplate($title, $value) {
			SystemInfo("title: ".$title."  value: ".$value, "template (ReplaceTemplate)");
			
			
			preg_match('#::'.$title.'\(([0-9]){1}\)::#', $this->TemplateContent, $regs, PREG_OFFSET_CAPTURE); 
			$delimiter = "";
			for($i =0; $i<$regs[1][0]; $i++)
				$delimiter .= "\t";
				
			$value = $delimiter.str_replace("\n","\n".$delimiter, $value);
			$this->TemplateContent = str_replace("::" . $title . "(".$regs[1][0].")::", $value, $this->TemplateContent);
		}
		
		
	}

?>