<?php 

	class mysql
	{	
		/** Variabeln f�r dieses Objekt */
		var $serverid 			= "";
		var $error				= "";
		var $lastreturn			= "";
		var $lastquery			= "";
		
		
		
		
		/**
		* Verbindet zur DB
		*
		* @param	keine
		* @since	Version 0.1
		**/
		function mysqlStart(){
			SystemInfo("class mysql starting", "mysql (mysqlStart)");
			
			
			if(_db_duration) $this->serverid = mysql_pconnect(_db_host, _db_user, _db_pass);
			else $this->serverid = mysql_connect(_db_host, _db_user, _db_pass);		
			if(!($this->serverid)){
				SystemWarning("Mysql Unable to Access the DB; DB-Host: \""._db_host."\" DB-User: \""._db_user."\"", "mysql (mysqlStart)");
				return false;
			}
			/** Datenbank ausw�hlen */
			if(!(mysql_select_db(_db_database))) {
				SystemWarning("Mysql Unable to Access the Table: \""._db_database."\"", "mysql (mysqlStart)");
				return false;
			}
			return true;
		}
		
		
		
		
		
		
		
		/**
		* erstellt eine neue Tabelle
		*
		* @param	Tabellennamen
		* @since	Version 0.1
		**/
		function Createtable($table, $table_name){
			$return = mysql_query($this->make_Createtable($table, $table_name), $this->serverid);
			$this->lastreturn = $return;
			SystemInfo($this->lastquery, "mysql (Createtable)");
			return $return;
		}
		
		
		/**
		* erstellt und f�hrt eine Insert sql query aus
		*
		* @param	string		Tabellen Ziel
		* @param	array		Werte ($tablecontent = array("Feld"=>"Wert","Irgendwas"=>"lol"))
		* @since	Version 0.1
		**/
		function Insertquery($table, $fields_values){	
			$this->lastreturn = mysql_query($this->make_Insertquery($table, $fields_values), $this->serverid);
			SystemInfo($this->lastquery, "mysql (Insertquery)");
			return $this->lastreturn;
		}
		
		/**
		* erstellt und f�hrt eine Select sql query aus
		*
		* @param	array		Array ($select = array('select'=>'ID,Irgendwas','from'=>'test','where'=>'ID<10','group'=>'','limit'=>'','order'=>'ID'))
		* @since	Version 0.1
		**/
		function Selectquery($select){
			$return = mysql_query($this->make_Selectquery($select), $this->serverid);
			$this->lastreturn = $return;
			SystemInfo($this->lastquery, "mysql (Selectquery)");
			return $return;
		}
		
		/**
		* erstellt und f�hrt eine UPDATE sql query aus
		*
		* @param	array		Array ($update = array('update'=>'table','set'=>'foo=\'bar\'','where'=>'ID=10','limit'=>'','order'=>''))
		* @since	Version 0.1
		**/
		function Updatequery($update){
			$return = mysql_query($this->make_Updatequery($update), $this->serverid);
			$this->lastreturn = $return;
			SystemInfo($this->lastquery, "mysql (Updatequery)");
			return $return;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/**
		 * Erstell ein Create Table SQL-Befehl
		 *
		 * @param	string		Tabellennamen
		 * @return	string		Full SQL query for INSERT (unless $fields_values does not contain any elements in which case it will be false)
		 * @deprecated			use Createtable() instead if possible!
		 */	
		function make_Createtable($table, $table_name, $quotes = false)
		{
			$query = "";
			$size = sizeof($table);
			$integer = 0;
			if(is_array($table) && count($table) && is_string($table_name)){
				$query = 	'CREATE TABLE '. $table_name. ' ( '."\n";
				foreach ($table as $key => $value)$query = $query ."\t".$value ."\t\t".$key.','."\n";
				$query = substr($query, 0, -2);
				$query = $query ."\n".');';
			}
			$this->lastquery = $query;
			return $query;
		}
		
		/**
		 * Erstell ein Insert SQL-Befehl
		 *
		 * @param	string		lool Insertquery()
		 * @param	array		look Insertquery()
		 * @param	string/array		See fullQuoteArray()
		 * @return	string		Full SQL query for INSERT (unless $fields_values does not contain any elements in which case it will be false)
		 * @deprecated			use INSERTquery() instead if possible!
		 */	
		function make_Insertquery($table, $fields_values, $quotes = false)
		{
			$fields_values = $this->fullQuoteArray($fields_values,$table,$quotes);
			if(is_array($fields_values) && count($fields_values)) $query = 	'INSERT INTO '. $table .' ( '."\n".implode(', ',array_keys($fields_values)).' )'."\n".' VALUES ( '.implode(', ',$fields_values).' )';
			$this->lastquery = $query;
			return $query;
		}
		
		/**
		 * Erstell ein Select SQL-Befehl
		 *
		 * @param	array		look Selectquery()
		 * @return	string		Full SQL query for Select
		 * @deprecated			use Selectquery() instead if possible!
		 */	
		function make_Selectquery($select){
			$query = 'SELECT '.$select["select"].' FROM '.$select["from"];
			if (strlen($select["where"])>0) $query.= ' WHERE '.$select["where"]."\n";
			if (strlen($select["group"])>0) $query.= ' GROUP BY '.$select["group"]."\n";
			if (strlen($select["order"])>0)	$query.= ' ORDER BY '.$select["order"]."\n";
			if (strlen($select["limit"])>0)	$query.= ' LIMIT '.$select["limit"]."\n";
			$this->lastquery = $query;		
			return $query;
		}
	
		
		/**
		 * Erstell ein Update SQL-Befehl
		 *
		 * @param	array		look Updatequery()
		 * @return	string		Full SQL query for Select
		 * @deprecated			use Selectquery() instead if possible!
		 */	
		function make_Updatequery($update){
			$query = 'UPDATE '.$update["update"].' SET '.$update["set"]. (strlen($update["where"])>0 ? "\n".' WHERE '.$update["where"]."\n" : '');
			if (strlen($update["order"])>0)	$query.= 'ORDER BY '.$update["order"]."\n";
			if (strlen($update["limit"])>0)	$query.= 'LIMIT '.$update["limit"]."\n";
			$this->lastquery = $query;		
			return $query;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/**
		 * Will fullquote all values in the one-dimensional array so they are ready to "implode" for an sql query.
		 *
		 * @param	array		Array with values (either associative or non-associative array)
		 * @param	string		Table name for which to quote
		 * @param	string/array		List/array of keys NOT to quote (eg. SQL functions) - ONLY for associative arrays
		 * @return	array		The input array with the values quoted
		 * @see cleanIntArray()
		 * @since	Version 0.1
		 */
		function fullQuoteArray($arr, $table, $Quote=FALSE)	{
			if (is_string($Quote)) $Quote = explode(',',$Quote);
			elseif (!is_array($Quote)) $Quote = FALSE;
			foreach($arr as $k => $v) if ($Quote===FALSE || !in_array($k,$Quote)) $arr[$k] = $this->fullQuoteStr($v, $table);
			return $arr;
		}
		
		/**
		 * Escaping and quoting values for SQL statements.
		 *
		 * @param	string		Input string
		 * @param	string		Table name for which to quote string. Just enter the table that the field-value is selected from (and any DBAL will look up which handler to use and then how to quote the string!).
		 * @return	string		Output string; Wrapped in single quotes and quotes in the string (" / ') and \ will be backslashed (or otherwise based on DBAL handler)
		 * @see quoteStr()
		 * @since	Version 0.1
		 */
		function fullQuoteStr($str, $table)	{
			return '\''.mysql_real_escape_string($str, $this->serverid).'\'';
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/**
		* Beendet eine DB-Verbindung, sofern diese nicht dauerhaft ist.
		*
		* @param	keine
		* @since	Version 0.1
		**/
		public function shut() 
		{
			SystemInfo("shut", "mysql (shut)");
			if(!($this->serverid)) {
				SystemWarning("Mysql DB Connection not exists");
				return false;
			}
			else{
				@mysql_close($this->serverid);
				return true;
			}
		}
		
		
		
		
		
		
		/**
		 * Gibt den Error-Status des letzten sql() Aufruf zur�ck.
		 * mysql_error() wrapper function
		 *
		 * @return	string		MySQL error string.
		 * @since 	Version 0.1
		 */
		function sql_error()	{
			return mysql_error($this->serverid);
		}
	}
?>