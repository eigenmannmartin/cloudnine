<?php
	class url{
		
		/* var $URL
		 * 
		 * array
			  0 => string 'foo/bar/index.php?url=blalba&uri=lol&uri1=lol&uri2=lol&uri3=lol&uri4=lol&uri5=lol&uri6=lol' (length=90)
			  1 => 
			    array
			      0 => string 'index' (length=5)
			      1 => string '.php' (length=4)
			  2 => 
			    array
			      0 => string 'foo' (length=3)
			      1 => string 'bar' (length=3)
			      2 => string 'index.php?url=blalba&uri=lol&uri1=lol&uri2=lol&uri3=lol&uri4=lol&uri5=lol&uri6=lol' (length=82)
		 */
		var $URL;
		
		
		/**
		* initialise the URL
		*
		* @param	none
		* @return	none
		* @since	Version 0.2
		**/
		function url(){
			SystemInfo("class url starting", "url (url)");
			$this->GetPage($this->SplitURL());					
		}
		
		/**
		* split the URL
		*
		* @param	none
		* @return	none
		* @since	Version 0.2
		**/
		function SplitURL(){
			if(empty($_SERVER['REQUEST_URI'])){
				SystemInfo("_SERVER['REQUEST_URI']: ".$_SERVER['REQUEST_URI'], "url (SplitURL)");
				return $this->URL = FALSE;
			}else {
				if(preg_match('#[a-zA-Z0-9���\/.]#',$_SERVER['REQUEST_URI']))
				//if(ereg ("^[a-zA-Z0-9���\/.]*$",$_SERVER['REQUEST_URI']))
					$this->URL[0] = $_SERVER['REQUEST_URI']; //"foo/bar/index.php?url=blalba&uri=lol&uri1=lol&uri2=lol&uri3=lol&uri4=lol&uri5=lol&uri6=lol"
				else{
					SystemWarning("URL not valid: ".$_SERVER['REQUEST_URI'], "url (SplitURL)");
					return FALSE;
				}
					
			}
			
			$this->URL[2] = explode("/",substr($this->URL[0],1));
			$aCount = count($this->URL[2]);
			if(!$aQMPos = strpos($this->URL[2][($aCount-1)], "?")){
				$aQMPos = strlen($this->URL[2][($aCount-1)]);
			}
			$this->URL[2][($aCount)] = substr($this->URL[2][($aCount-1)], $aQMPos );
			$this->URL[2][($aCount-1)] = substr($this->URL[2][($aCount-1)], 0, $aQMPos);

			
			
			
			$aDotPos = strpos($this->URL[2][($aCount-1)],".");
			$this->URL[1][0] = substr($this->URL[2][($aCount-1)],0,$aDotPos);
			$this->URL[1][1] = substr($this->URL[2][($aCount-1)],$aDotPos,($aDotPos));
			
			
			
			return TRUE;
		}
		
		/**
		* gets the page
		*
		* @param	none
		* @return	none
		* @since	Version 0.2
		**/
		function GetPage($url = FALSE){

			
			$this->URL[2][0] = $this->GetPageHelper($this->URL[2][0]);
			
			if(defined($this->URL[2][0])){ 
				SystemInfo("URL found, using: \"".$this->URL[2][0]."\"", "url (GetPage)"); 
				return TRUE;
			}else{
				SystemWarning("URL not found, using default! Faild with: ".$this->URL[2][0], "url (GetPage)");
				if(!defined($this->URL[2][0] = $this->GetPageHelper("default"))){
					SystemError("URL not found, using default failed: ".$this->URL[2][0], "url (GetPage)");
					return FALSE;
				}
				return TRUE;
			}
		}
		
		function GetPageHelper($url){
			$page = "";
			$tmp = "";
			while(defined($tmp = $url)){
				$page = $tmp;
				$tmp = constant($url);
				$url = $tmp;
			}
			return $page;
		}

	}
?>