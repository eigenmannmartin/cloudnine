<?php
	abstract class language {
		
		private static $Languages = array();
		
		
		
		public static function getWords($word, $language = _defLang){
			
		
			if(!array_key_exists($language, self::$Languages)){
				SystemWarning('Language: "'.$language.'" is not supported', 'getWord (language)');
				$language = _defLang;
			}
			
			if(!array_key_exists($word, self::$Languages[$language])){
				SystemWarning('Word: "'.$word.'" is not supported', 'getWord (language)');
			}else{
				return self::$Languages[$language][$word];
			}
		}
		
		
		
		 public static function init(){
			
			self::$Languages = array(
				'de' => array(
						'password' => 'Passwort',
						'username' => 'Benutzername',
						'send' => 'senden',
						'logout' => 'ausloggen',
						'login' => 'einloggen',
			
						'hello' => 'Hallo',
				
						),
				'en' => array(
						'password' => 'password',
						'username' => 'username',
						'send' => 'send',
						'logout' => 'logout',
						'login' => 'login',
						
						'hello' => 'hello',
				
				)
			);
		}
	}


?>